class CreateStatements < ActiveRecord::Migration
  def change
    create_table :statements do |t|
      t.date :date
      t.string :reason
      t.integer :kms
      t.string :road
      t.string :place
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
