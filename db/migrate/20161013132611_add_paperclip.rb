class AddPaperclip < ActiveRecord::Migration
  def change
    add_attachment :statements, :picture
  end
end
