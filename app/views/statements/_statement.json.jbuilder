json.extract! statement, :id, :date, :reason, :kms, :road, :place, :user_id, :created_at, :updated_at
json.url statement_url(statement, format: :json)