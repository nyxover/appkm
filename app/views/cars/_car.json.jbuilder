json.extract! car, :id, :brand, :type, :horsepower, :user, :created_at, :updated_at
json.url car_url(car, format: :json)